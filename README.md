# LIS4368 Advanced Web Applications Development

## Christian Giordano

### LIS4368 Requirements:

*Course Work Links:*

[A1 README.md](a1/README.md "My A1 README.md file")

- Install JDK
- Install Tomcat
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutorials
- Provide git command descriptions

[A2 README.md](a2/README.md "My A2 README.md file")

- Display localhost hello
- Display localhost hello/HelloHome
- Display localhost hello/sayhello
- Display localhost hello/querybook
- Connect MySQL database to webpage

[A3 README.md](a3/README.md "My A3 README.md file")

- Create an ERD
- Populate the ERD
- Display a Screenshot of the ERD on the A3 index page of the site

[P1 README.md](p1/README.md "My P1 README.md file")

- Create Basic client-side validation
- Show correct and incorrect entries
- Display a screenshot of the Main Splash page

[A5 README.md](a5/README.md "My A5 README.md file")

- Receive data from user
- Check data
- Display data inputted
- Place data into a database

[P2 README.md](p2/README.md "My P2 README.md file")

- Create Basic server-side validation
- Create, edit, and delete entries from the table
- Display a screenshots of all steps