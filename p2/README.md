# LIS4368 Advanced Web Applications development

## Christian Giordano

### Project 2 Requirements:

1. Create Basic server-side validation
2. Create, edit, and delete entries from the table
3. Display a screenshots of all steps

#### README.md file should include the following items:

* Screenshots of valid user entry
* Screenshots of passed validation
* Screenshots of displayed data
* Screenshots of modify form
* Screenshots of modified data
* Screenshots of delete warning
* Screenshots of cmd with select, insert, update, and delete

#### Assignment Screenshots:

Screenshots of valid user entry

![web2a](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2a.png)

Screenshots of passed validation

![webp2thanks](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2thanks.png)



Screenshots of displayed data

![webp2data](https://bitbucket.org/cjg18/lis4368/raw/5c79b2c6af697df0f4279ba397958d59d75e6751/p2/photos/webp2data.png)

Screenshots of modify form

![webp2edit](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2edit.png)

Screenshots of modified data

![webp2edit2](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2edit2.png)

Screenshots of delete warning

![webp2del](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2del.png)

Screenshots of cmd with select, insert, update, and delete

![webp2cmd1](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2cmd1.png)

![webp2cmd2](https://bitbucket.org/cjg18/lis4368/raw/aeb678bc2e4b6c334bcac619a8767f15c0ce4768/p2/photos/webp2cmd2.png)