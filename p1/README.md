# LIS4368 Advanced Web Applications development

## Christian Giordano

### Project 1 Requirements:

1. Create Basic client-side validation
2. Show correct and incorrect entries
3. Display a screenshot of the Main Splash page

#### README.md file should include the following items:

* Screenshots Show correct and incorrect entries
* Display a screenshot of the Main Splash page

#### Assignment Screenshots:

Screenshot of the Main Splash Page:

![home](https://bitbucket.org/cjg18/lis4368/raw/f7675a425bb231f8bd64cddb0e506f382ab11386/p1/pictures/home.png)

Screenshot of incorrect data entry:

![failed1](https://bitbucket.org/cjg18/lis4368/raw/f7675a425bb231f8bd64cddb0e506f382ab11386/p1/pictures/failed1.png)

![failed2](https://bitbucket.org/cjg18/lis4368/raw/f7675a425bb231f8bd64cddb0e506f382ab11386/p1/pictures/failed2.png)

Screenshot of correct data entry:

![correct](https://bitbucket.org/cjg18/lis4368/raw/f7675a425bb231f8bd64cddb0e506f382ab11386/p1/pictures/correct.png)
