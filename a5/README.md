# LIS4368 Advanced Web Applications development

## Christian Giordano

### Assignment #5 Requirements:

1. Receive data from user
2. Check data
3. Display data inputted
4. Place data into a database

#### README.md file should include the following items:

* Screenshot of inputs waiting
* Screenshot of data displayed to user
* Screenshot of data in the database
* Screenshots of SS13-15

#### Assignment Screenshots:

Screenshot of inputs waiting:

![a5home](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/a5home.png)

Screenshot of data displayed to user:

![a5list](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/a5list.png)

Screenshot of data in the database:

![a5data](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/a5data.PNG)

Screenshot of SS13:

![webss13](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/webss13.png)

Screenshot of SS14:

![webss14](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/webss14.png)

Screenshot of SS15:

![webss15](https://bitbucket.org/cjg18/lis4368/raw/046ade95e6dad92a5c97683b87e0ec3cbf898989/a5/photos/webss15.png)

