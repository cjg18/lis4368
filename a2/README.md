# LIS4368 Advanced Web Applications development

## Christian Giordano

### Assignment #2 Requirements:

1. Display localhost hello
2. Display localhost hello/HelloHome
3. Display localhost hello/sayhello
4. Display localhost hello/querybook
5. Connect MySQL database to webpage

#### README.md file should include the following items:

* Screenshot localhost hello
* Screenshot localhost hello/HelloHome
* Screenshot localhost hello/sayhello
* Screenshot localhost hello/querybook

#### Assignment Screenshots:

*Screenshot of http://localhost:9999/hello/index.html

![hello](https://bitbucket.org/cjg18/lis4368/raw/79f42262b7b81fbc9bd8417d03446a46f28495a0/a2/pictures/hello.png)

*Screenshot of http://localhost:9999/hello/sayhello:

![sayhello](https://bitbucket.org/cjg18/lis4368/raw/79f42262b7b81fbc9bd8417d03446a46f28495a0/a2/pictures/sayhello.png)

*Screenshot of http://localhost:9999/hello/querybook.html:

![oneselected](https://bitbucket.org/cjg18/lis4368/raw/79f42262b7b81fbc9bd8417d03446a46f28495a0/a2/pictures/oneselected.png)

Screenshot of query results:

![query](https://bitbucket.org/cjg18/lis4368/raw/79f42262b7b81fbc9bd8417d03446a46f28495a0/a2/pictures/query.png)

Screenshot of website with query results:

![a2website1](https://bitbucket.org/cjg18/lis4368/raw/16b326d6a3ac781af3ccb365b8000425604da5c2/a2/pictures/a2website1.PNG)

![a2website1](https://bitbucket.org/cjg18/lis4368/raw/16b326d6a3ac781af3ccb365b8000425604da5c2/a2/pictures/a2website2.PNG)