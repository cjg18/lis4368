> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS Advanced Web Applications development

## Christian Giordano

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Java/SP/Servlet Development Installation
3. Chapter Questions (chp 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running https://localhost9999
* git commands w/short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init – start a new repository.
2. git status – List all files that have been committed.
3. git add – Adds a file to the “staging area”.
4. git commit – Records the file permanently .
5. git push – sends the committed changes to the remote repository.
6. git pull – pulls data from remote repository and places it in your local directory.
7. git clone – allows you to obtain a repository from an existing URL.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/ampps1.png)

![AMPPS Installation Screenshot2](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/ampps2.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/HelloWorld.png)

*Screenshot of local host:

![Android Studio Installation Screenshot](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/localhost9999.png)

Screenshot of homepage

![Android Studio Installation Screenshot](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/homepage.PNG)

Screenshot of Assignment 1 page

![Android Studio Installation Screenshot](https://bitbucket.org/cjg18/lis4368/raw/ce2d2981de4a2126f46328c6952c257df17286c6/a1/pictures/a1page.PNG)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/cjg18/bitbucketstationlocations/ "Bitbucket Station Locations")


