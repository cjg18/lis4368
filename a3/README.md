# LIS4368 Advanced Web Applications development

## Christian Giordano

### Assignment #3 Requirements:

1. Create an ERD
2. Populate the ERD
3. Display a screenshot of the ERD on the A3 index page of the site

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of a3/index.jsp
* Links to the files a3.mwb and a3.sql

#### Assignment Screenshots:

Screenshot of the ERD

![a3erd](https://bitbucket.org/cjg18/lis4368/raw/25778658620953ac819884f3814064955dbb9f1d/a3/pictures/a3erd.png)

Screenshot of the ERD on the a3/index.jsp

![a3index](https://bitbucket.org/cjg18/lis4368/raw/25778658620953ac819884f3814064955dbb9f1d/a3/pictures/a3index.png)





#### Assignment Files:

a3 mwb: https://bitbucket.org/cjg18/lis4368/src/a55e896257a3934429b97444c905ce2bae6e8c60/a3/files/a3.mwb

a3 sql: https://bitbucket.org/cjg18/lis4368/src/a55e896257a3934429b97444c905ce2bae6e8c60/a3/files/a3sql.sql